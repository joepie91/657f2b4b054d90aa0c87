function createUser(name, height, properties) {
	return Object.assign({
		name: name,
		height: height,
		noise: "hello world!",
		makeNoise: function() {
			console.log(this.noise);
		}
	}, properties);
}

var user = createUser("joepie91", "1.94m", {
	noise: "hallo!"
});